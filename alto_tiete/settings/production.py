# coding: utf-8
import os

from .base import *
from .utils import gen_get_config_or, read_config_file

config = read_config_file()
get_config_or = gen_get_config_or(config)

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['segrega.lidas.org.br', 'segrega.net.br']

SECRET_KEY = get_config_or('APP', 'secret_key', SECRET_KEY)

DJANGO_HOME = os.path.join('/home', 'django')
STATIC_DIR = os.path.join(DJANGO_HOME, 'static_files', 'segrega')
MEDIA_ROOT = os.path.join(STATIC_DIR, 'media')
STATIC_ROOT = os.path.join(STATIC_DIR, 'static')

DATABASE_NAME = get_config_or('DATABASE', 'NAME', default='')
DATABASE_USER = get_config_or('DATABASE', 'USER', default='')
DATABASE_PASS = get_config_or('DATABASE', 'PASSWORD', default='')
DATABASE_HOST = get_config_or('DATABASE', 'HOST', default='localhost')
DATABASE_PORT = get_config_or('DATABASE', 'PORT', default='5432')

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'segrega',
        'USER': 'segrega_user',
        'PASSWORD': 's3gr3g4',
        'HOST': 'localhost',
        'PORT': 5432,
        'CONN_MAX_AGE': 600,
    },
}


