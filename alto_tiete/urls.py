from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import admin
from alto_tiete import settings

from django.contrib.auth import views as auth_views
from django import views as django_views
from django.conf.urls.static import static

urlpatterns = [
	url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
	url(r'^admin/', admin.site.urls),

    url(r'^$', TemplateView.as_view(template_name='home.html'), name='index'),

    url(r'^accounts/login/$', auth_views.login),
    
    url(r'^entrar/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^sair/$', auth_views.logout, {'next_page': reverse_lazy('login')}, name='logout'),

    url(r'^produtos/$', TemplateView.as_view(template_name='produtos.html'), name='produtos'),

    # Apps
    url(r'^mapa/', include('mapa.urls')),
    url(r'^ciclo/', include('ciclo.urls')),
    url(r'^cadastro/', include('cadastro.urls')),
    url(r'^comercializacao/', include('comercializacao.urls')),
    
    
]
