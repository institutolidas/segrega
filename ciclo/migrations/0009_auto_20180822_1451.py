# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-08-22 17:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ciclo', '0008_auto_20180810_2124'),
    ]

    operations = [
        migrations.AddField(
            model_name='adubacao',
            name='lote',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='fitossanidade',
            name='lote',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='preparosolo',
            name='lote',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
