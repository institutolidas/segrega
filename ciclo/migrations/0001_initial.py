# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2018-02-09 14:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mapa', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cadastro', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Adubacao',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('data_adubacao', models.DateField(blank=True, null=True)),
                ('quantidade', models.FloatField(blank=True, null=True)),
                ('horas_maquina', models.FloatField(blank=True, null=True)),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
                ('equipamento', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Equipamento')),
                ('insumo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Insumo')),
                ('propriedade', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade')),
                ('talhao_gleba', models.ManyToManyField(blank=True, to='mapa.TalhoesGlebas')),
            ],
            options={
                'verbose_name': 'Aduba\xe7\xe3o',
                'verbose_name_plural': 'Aduba\xe7\xf5es',
            },
        ),
        migrations.CreateModel(
            name='Colheita',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('lote_prop', models.IntegerField(blank=True, null=True)),
                ('data_colheita', models.DateField(blank=True, null=True)),
                ('unidades', models.IntegerField(blank=True, null=True)),
                ('tipo_unidade', models.CharField(choices=[('1', 'Ma\xe7o(s)'), ('2', 'Unidade(s)'), ('3', 'Caixa(s)'), ('4', 'Kg'), ('5', 'Bandeja(s)'), ('6', 'D\xfazia(s)'), ('7', 'Cento(s)')], max_length=1, verbose_name='Tipo de unidade')),
                ('sobra', models.FloatField(blank=True, null=True)),
                ('status_lote', models.CharField(choices=[('1', 'Aberto'), ('2', 'Encerrado')], max_length=1, verbose_name='Status do Lote')),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Colheita',
                'verbose_name_plural': 'Colheitas',
            },
        ),
        migrations.CreateModel(
            name='EquipamentoPreparo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas_maquina', models.SmallIntegerField(blank=True, null=True)),
                ('equipamento', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Equipamento')),
            ],
        ),
        migrations.CreateModel(
            name='Fitossanidade',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('data_fito', models.DateField(blank=True, null=True)),
                ('praga_doenca', models.CharField(blank=True, max_length=250, null=True)),
                ('quantidade', models.FloatField(blank=True, null=True)),
                ('dosagem', models.FloatField(blank=True, null=True)),
                ('horas_maquina', models.FloatField(blank=True, null=True)),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
                ('equipamento', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Equipamento')),
                ('insumo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Insumo')),
                ('propriedade', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade')),
                ('talhao_gleba', models.ManyToManyField(blank=True, to='mapa.TalhoesGlebas')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Fitossanidade',
                'verbose_name_plural': 'Fitossanidade',
            },
        ),
        migrations.CreateModel(
            name='MdoAdubacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('adubacao', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='adubacao', to='ciclo.Adubacao')),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='MdoColheita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('colheita', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='colheita', to='ciclo.Colheita')),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='MdoFitossanidade',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('fitossanidade', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fitossanidade', to='ciclo.Fitossanidade')),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='MdoPlantio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='MdoPreparo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='MdoSemeio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horas', models.SmallIntegerField(blank=True, null=True)),
                ('nome', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.MaoDeObra')),
            ],
        ),
        migrations.CreateModel(
            name='Plantio',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('lote_prop', models.IntegerField(blank=True, null=True)),
                ('data_plantio', models.DateField(null=True)),
                ('origem', models.CharField(choices=[('1', 'Viveiro'), ('2', 'Compra'), ('3', 'N\xe3o Identificado')], max_length=1, verbose_name='Origem')),
                ('unidades', models.IntegerField(blank=True, null=True)),
                ('restantes', models.IntegerField(blank=True, null=True)),
                ('status_lote', models.CharField(choices=[('1', 'Aberto'), ('2', 'Encerrado')], max_length=1, verbose_name='Status do Lote')),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Plantio',
                'verbose_name_plural': 'Plantios',
            },
        ),
        migrations.CreateModel(
            name='PreparoSolo',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('data_preparo', models.DateField(blank=True, null=True)),
                ('quantidade', models.FloatField(blank=True, null=True)),
                ('horas_maquina', models.FloatField(blank=True, null=True)),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
                ('equipamento', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Equipamento')),
                ('insumo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Insumo')),
                ('propriedade', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade')),
                ('talhao_gleba', models.ManyToManyField(blank=True, to='mapa.TalhoesGlebas')),
            ],
            options={
                'verbose_name': 'Preparo do Solo',
                'verbose_name_plural': 'Prepara\xe7\xe3o do Solo',
            },
        ),
        migrations.CreateModel(
            name='Semeio',
            fields=[
                ('lote', models.AutoField(primary_key=True, serialize=False)),
                ('lote_prop', models.IntegerField(blank=True, null=True)),
                ('data_semeio', models.DateField(blank=True, null=True)),
                ('viveiro', models.CharField(blank=True, max_length=255, null=True)),
                ('n_bandejas', models.SmallIntegerField(blank=True, null=True)),
                ('unidades', models.IntegerField(blank=True, null=True)),
                ('restantes', models.IntegerField(blank=True, null=True)),
                ('status_lote', models.CharField(choices=[('1', 'Aberto'), ('2', 'Encerrado')], max_length=1, verbose_name='Status do Lote')),
                ('data_cadastro', models.DateTimeField(blank=True, null=True)),
                ('produto', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Produto')),
                ('propriedade', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Semeio',
                'verbose_name_plural': 'Semeios',
            },
        ),
        migrations.CreateModel(
            name='TipoAdubacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'ordering': ['tipo'],
                'verbose_name': 'Tipo de Aduba\xe7\xe3o',
                'verbose_name_plural': 'Tipos de Aduba\xe7\xe3o',
            },
        ),
        migrations.CreateModel(
            name='TipoPreparo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'ordering': ['tipo'],
                'verbose_name': 'Tipo de Preparo',
                'verbose_name_plural': 'Tipos de Preparo',
            },
        ),
        migrations.AddField(
            model_name='preparosolo',
            name='tipo_preparo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ciclo.TipoPreparo'),
        ),
        migrations.AddField(
            model_name='preparosolo',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='plantio',
            name='lote_semeio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ciclo.Semeio'),
        ),
        migrations.AddField(
            model_name='plantio',
            name='produto',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Produto'),
        ),
        migrations.AddField(
            model_name='plantio',
            name='propriedade',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade'),
        ),
        migrations.AddField(
            model_name='plantio',
            name='talhao_gleba',
            field=models.ManyToManyField(blank=True, to='mapa.TalhoesGlebas'),
        ),
        migrations.AddField(
            model_name='plantio',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mdosemeio',
            name='semeio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='semeio', to='ciclo.Semeio'),
        ),
        migrations.AddField(
            model_name='mdopreparo',
            name='preparo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='preparo', to='ciclo.PreparoSolo'),
        ),
        migrations.AddField(
            model_name='mdoplantio',
            name='plantio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='plantio', to='ciclo.Plantio'),
        ),
        migrations.AddField(
            model_name='equipamentopreparo',
            name='preparo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ciclo.PreparoSolo'),
        ),
        migrations.AddField(
            model_name='colheita',
            name='lote_plantio',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ciclo.Plantio'),
        ),
        migrations.AddField(
            model_name='colheita',
            name='propriedade',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='cadastro.Propriedade'),
        ),
        migrations.AddField(
            model_name='colheita',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='adubacao',
            name='tipo_adubacao',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ciclo.TipoAdubacao'),
        ),
        migrations.AddField(
            model_name='adubacao',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
