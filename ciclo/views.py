# coding: utf-8

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ImproperlyConfigured
from django.core.urlresolvers import reverse_lazy, reverse
from django.forms import models as model_forms, ModelChoiceField
from django.forms.formsets import formset_factory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect, render, get_object_or_404, render_to_response
from django.contrib.admin.views.decorators import staff_member_required
from django.apps import apps
from django.db.models import Count, Sum

from django.contrib.auth.models import User

from django.utils import timezone
import datetime

from django.conf import settings

from cadastro.models import (
	PerfilUsuario,
	Equipamento,
	Propriedade,
	Insumo,
	Produto,
	MaoDeObra,
	)

from ciclo.models import (
	PreparoSolo,
	MdoPreparo,
	Adubacao,
	MdoAdubacao,
	Fitossanidade,
	MdoFitossanidade,
	Semeio,
	MdoSemeio,
	Plantio,
	MdoPlantio,
	Colheita,
	MdoColheita,
	)

from .forms import (
	PreparoSoloForm,
	MdoPreparoFormSet,
	AdubacaoForm,
	MdoAdubacaoFormSet,
	FitossanidadeForm,
	MdoFitossanidadeFormSet,
	SemeioForm,
	MdoSemeioFormSet,
	PlantioForm,
	MdoPlantioFormSet,
	ColheitaForm,
	MdoColheitaFormSet,
	)

from mapa.models import (
	TalhoesGlebas,
	)



@login_required
def preparosolo_cadastrar(request):

	form = PreparoSoloForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoPreparoFormSet(
		data=request.POST or None,
		request_user=request.user,
	)

	talhao_gleba = request.GET.get('talhao_gleba')
	form.fields["talhao_gleba"].initial = [talhao_gleba]

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			return HttpResponseRedirect(reverse_lazy('preparosolo_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset,
	}

	return render(request, 'ciclo/preparosolo_cadastrar.html', context)


@login_required
def preparosolo_listar(request):

	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = PreparoSolo.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/preparosolo_listar.html', context)


@login_required
def preparosolo_remover(request, id):

	lote = PreparoSolo.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Lote removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('preparosolo_listar'))


@login_required
def preparosolo_editar(request, id):

	lote = PreparoSolo.objects.get(pk=int(id))

	form = PreparoSoloForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoPreparoFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Lote editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('preparosolo_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/preparosolo_editar.html', context)


@login_required
def adubacao_cadastrar(request):

	form = AdubacaoForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoAdubacaoFormSet(
		data=request.POST or None,
		request_user=request.user,
	)
	
	talhao_gleba = request.GET.get('talhao_gleba')
	form.fields["talhao_gleba"].initial = [talhao_gleba]

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Adubação cadastrada com sucesso')
			return HttpResponseRedirect(reverse_lazy('adubacao_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/adubacao_cadastrar.html', context)


@login_required
def adubacao_listar(request):

	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = Adubacao.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/adubacao_listar.html', context)


@login_required
def adubacao_remover(request, id):

	lote = Adubacao.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Adubação removida com sucesso')
	return HttpResponseRedirect(reverse_lazy('adubacao_listar'))


@login_required
def adubacao_editar(request, id):

	lote = Adubacao.objects.get(pk=int(id))

	form = AdubacaoForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoAdubacaoFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Adubação editada com sucesso')
			return HttpResponseRedirect(reverse_lazy('adubacao_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/adubacao_editar.html', context)


@login_required
def fitossanidade_cadastrar(request):

	form = FitossanidadeForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoFitossanidadeFormSet(
		data=request.POST or None,
		request_user=request.user,
	)

	talhao_gleba = request.GET.get('talhao_gleba')
	form.fields["talhao_gleba"].initial = [talhao_gleba]

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Aplicação cadastrada com sucesso')
			return HttpResponseRedirect(reverse_lazy('fitossanidade_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/fitossanidade_cadastrar.html', context)


@login_required
def fitossanidade_listar(request):

	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = Fitossanidade.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/fitossanidade_listar.html', context)


@login_required
def fitossanidade_remover(request, id):

	lote = Fitossanidade.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Aplicação removida com sucesso')
	return HttpResponseRedirect(reverse_lazy('fitossanidade_listar'))


@login_required
def fitossanidade_editar(request, id):

	lote = Fitossanidade.objects.get(pk=int(id))

	form = FitossanidadeForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoFitossanidadeFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Aplicação editada com sucesso')
			return HttpResponseRedirect(reverse_lazy('fitossanidade_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/fitossanidade_editar.html', context)


@login_required
def semeio_cadastrar(request):

	form = SemeioForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoSemeioFormSet(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()
			
			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Lote cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('semeio_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/semeio_cadastrar.html', context)


@login_required
def ajax_celulas(request, id_produto):
    '''Carrega combo ajax celulas'''

    produto = Produto.objects.get(pk=int(id_produto))

    celulas = produto.celulas_bandeja

    res = ''
    res = res + """<option value="%s""" % celulas

    return HttpResponse(celulas)


@login_required
def semeio_listar(request):

	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = Semeio.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/semeio_listar.html', context)


@login_required
def semeio_remover(request, id):

	lote = Semeio.objects.get(pk=int(id))
	plantio = Plantio.objects.filter(lote_semeio=id)
	colheita = Colheita.objects.filter(lote_plantio=plantio)
	
	lote.delete()
	plantio.delete()
	colheita.delete()

	messages.success(request, u'Lote removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('semeio_listar'))


@login_required
def semeio_editar(request, id):

	lote = Semeio.objects.get(pk=int(id))

	form = SemeioForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoSemeioFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Lote editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('semeio_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/semeio_editar.html', context)


@login_required
def semeio_fechar_abrir(request, id):
	
	lote = Semeio.objects.get(pk=int(id))
	
	if lote.status_lote == '1':
		lote.status_lote = '2'
		lote.save()

	elif lote.status_lote == '2':
		lote.status_lote = '1'
		lote.save()

	messages.success(request, u'Lote fechado/aberto com sucesso')
	return HttpResponseRedirect(reverse_lazy('semeio_listar'))


@login_required
def plantio_cadastrar(request):

	form = PlantioForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoPlantioFormSet(
		data=request.POST or None,
		request_user=request.user,
	)

	talhao_gleba = request.GET.get('talhao_gleba')
	form.fields["talhao_gleba"].initial = [talhao_gleba]

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()
			
			mdo_formset.instance = f
			mdo_formset.save()
			
			messages.success(request, u'Lote cadastrado com sucesso')
			return HttpResponseRedirect(reverse_lazy('plantio_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset,
	}

	return render(request, 'ciclo/plantio_cadastrar.html', context)


@login_required
def ajax_produto(request, id):
    '''Carrega combo ajax produto'''

    lote = Semeio.objects.get(pk=int(id))

    produto = lote.produto_id

    return HttpResponse(produto)


@login_required
def plantio_listar(request):
	
	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = Plantio.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/plantio_listar.html', context)


@login_required
def plantio_relatorio(request):
	
	lote = Plantio.objects.get_queryset_by_user(request.user)

	total_unidades = lote.aggregate(Sum('unidades'))

	unidade_produto = lote.values('produto__produto','produto__tipo').annotate(Sum('unidades')).order_by('produto__produto')

	context = {
		'lote': lote,
		'total_unidades': total_unidades,
		'unidade_produto': unidade_produto,
	}

	return render(request, 'ciclo/plantio_relatorio.html', context)


@login_required
def plantio_fechar_abrir(request, id):

	lote = Plantio.objects.get(pk=int(id))
	
	if lote.status_lote == '1':
		lote.status_lote = '2'
		lote.save()

	elif lote.status_lote == '2':
		lote.status_lote = '1'
		lote.save()

	messages.success(request, u'Lote fechado/aberto com sucesso')
	return HttpResponseRedirect(reverse_lazy('plantio_listar'))


@login_required
def plantio_remover(request, id):

	lote = Plantio.objects.get(pk=int(id))
	colheita = Colheita.objects.filter(lote_plantio=lote)

	lote.delete()
	colheita.delete()

	messages.success(request, u'Lote removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('plantio_listar'))


@login_required
def plantio_editar(request, id):

	lote = Plantio.objects.get(pk=int(id))

	form = PlantioForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoPlantioFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Plantio editada com sucesso')
			return HttpResponseRedirect(reverse_lazy('plantio_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/plantio_editar.html', context)


@login_required
def colheita_cadastrar(request):

	form = ColheitaForm(
		data=request.POST or None,
		request_user=request.user,
	)
	mdo_formset = MdoColheitaFormSet(
		data=request.POST or None,
		request_user=request.user,
	)

	if request.POST:
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()
			
			mdo_formset.instance = f
			mdo_formset.save()
			
			return HttpResponseRedirect(reverse_lazy('colheita_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/colheita_cadastrar.html', context)


@login_required
def ajax_lote_prop_colheita(request, id):
    '''Carrega ultimo lote de colheita com base no lote do plantio '''

    lote = Colheita.objects.filter(lote_plantio_id=int(id)).order_by('-lote').first()

    if lote is None:
    	lote = 1
    else:
    	lote = lote.lote + 1

    return HttpResponse(lote)

@login_required
def colheita_listar(request):

	order = request.GET.get('order')

	if order is None:
		order='-lote'

	lote = Colheita.objects.get_queryset_by_user(request.user).order_by(order)

	context = {
		'lote': lote,
	}

	return render(request, 'ciclo/colheita_listar.html', context)

@login_required
def colheita_relatorio(request):
	
	lote = Colheita.objects.get_queryset_by_user(request.user)

	total_unidades = lote.aggregate(Sum('unidades'))

	unidade_produto = lote.values('lote_plantio__produto__produto','lote_plantio__produto__tipo','tipo_unidade').annotate(Sum('unidades')).order_by('lote_plantio__produto__produto')

	context = {
		'lote': lote,
		'total_unidades': total_unidades,
		'unidade_produto': unidade_produto,
	}

	return render(request, 'ciclo/colheita_relatorio.html', context)

@login_required
def colheita_fechar_abrir(request, id):

	lote = Colheita.objects.get(pk=int(id))
	
	if lote.status_lote == '1':
		lote.status_lote = '2'
		lote.save()

	elif lote.status_lote == '2':
		lote.status_lote = '1'
		lote.save()

	messages.success(request, u'Lote fechado/aberto com sucesso')
	return HttpResponseRedirect(reverse_lazy('colheita_listar'))


@login_required
def colheita_remover(request, id):

	lote = Colheita.objects.get(pk=int(id))

	lote.delete()

	messages.success(request, u'Lote removido com sucesso')
	return HttpResponseRedirect(reverse_lazy('colheita_listar'))


@login_required
def colheita_editar(request, id):

	lote = Colheita.objects.get(pk=int(id))

	form = ColheitaForm(request.POST or None, instance=lote, request_user=request.user)
	mdo_formset = MdoColheitaFormSet(request.POST or None, instance=lote, request_user=request.user)

	if request.method == 'POST':
		
		if form.is_valid() and mdo_formset.is_valid():

			f = form.save()

			mdo_formset.instance = f
			mdo_formset.save()

			messages.success(request, u'Lote editado com sucesso')
			return HttpResponseRedirect(reverse_lazy('colheita_listar'))

	context = {
		'form': form,
		'mdo_formset': mdo_formset
	}

	return render(request, 'ciclo/colheita_editar.html', context)


@login_required
def talhaogleba_detalhe(request, talhao):

	talhao_gleba=TalhoesGlebas.objects.get(pk=int(talhao))
	adubacao=Adubacao.objects.filter(talhao_gleba__pk=int(talhao)).order_by('data_adubacao')
	preparosolo=PreparoSolo.objects.filter(talhao_gleba__pk=int(talhao)).order_by('data_preparo')
	plantio=Plantio.objects.filter(talhao_gleba__pk=int(talhao)).order_by('data_plantio')
	fitossanidade=Fitossanidade.objects.filter(talhao_gleba__pk=int(talhao)).order_by('data_fito')
	colheita=Colheita.objects.filter(lote_plantio__talhao_gleba__pk=int(talhao)).order_by('data_colheita')

	context = {
		'adubacao': adubacao,
		'preparosolo': preparosolo,
		'plantio': plantio,
		'colheita': colheita,
		'fitossanidade': fitossanidade,
		'talhao_gleba': talhao_gleba,
	}

	template_name = "ciclo/talhaogleba_detalhe.html"

	return render(request, template_name, context)