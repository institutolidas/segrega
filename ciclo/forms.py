# -*- coding: utf-8  -*-

from django.forms import inlineformset_factory
from django import forms
from django.contrib.auth.models import User
from django.utils import timezone
from django.forms import ValidationError

from cadastro.models import (
	Equipamento,
	Propriedade,
	Insumo,
	Produto,
	MaoDeObra,
	)

from mapa.models import (
    TalhoesGlebas,
    )

from ciclo.models import (
	PreparoSolo,
	MdoPreparo,
	EquipamentoPreparo,
	Adubacao,
	MdoAdubacao,
	Fitossanidade,
	MdoFitossanidade,
	Semeio,
	MdoSemeio,
	Plantio,
	MdoPlantio,
	Colheita,
	MdoColheita,
	)


class PreparoSoloForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(PreparoSoloForm, self).__init__(*args, **kwargs)

		self.fields["equipamento"].queryset = Equipamento.objects.get_queryset_by_user(self.request_user)
		self.fields["insumo"].queryset = Insumo.objects.get_queryset_by_user(self.request_user).filter(insumo__tipo="1")
		
		self.fields["talhao_gleba"].queryset = TalhoesGlebas.objects.get_queryset_by_user(self.request_user).order_by('id')
		
		qs = PreparoSolo.objects.get_queryset_by_user(self.request_user).order_by('-lote').first()
		
		if qs is None:
			self.fields["lote"].initial = 1
		else:
			lote = qs.lote
			self.fields["lote"].initial = lote + 1


	class Meta:
		model = PreparoSolo
		fields = '__all__'
		exclude = ['user']
		widgets = {
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'data_preparo': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'tipo_preparo': forms.Select(attrs={'class': 'form-control'}),
			'equipamento': forms.Select(attrs={'class': 'form-control'}),
			'talhao_gleba': forms.SelectMultiple(attrs={'class': 'form-control', 'size':'7'}),
			'horas_maquina': forms.NumberInput(attrs={'class': 'form-control'}),
			'insumo': forms.Select(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def clean_lote(self):
		lote = self.cleaned_data['lote']
		if PreparoSolo.objects.get_queryset_by_user(self.request_user).filter(lote=lote).exists():
			raise ValidationError("Já existe um lote cadastrado com esse valor")
		return lote


	def save(self, commit=True):
		instance = super(PreparoSoloForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BasePreparoFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BasePreparoFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BasePreparoFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form


MdoPreparoFormSet = inlineformset_factory(
	parent_model = PreparoSolo,
	model = MdoPreparo,
	formset = BasePreparoFormSet,
	fk_name='preparo',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)


class AdubacaoForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(AdubacaoForm, self).__init__(*args, **kwargs)

		self.fields["equipamento"].queryset = Equipamento.objects.get_queryset_by_user(self.request_user)
		self.fields["insumo"].queryset = Insumo.objects.get_queryset_by_user(self.request_user).filter(insumo__tipo="1")
		self.fields["talhao_gleba"].queryset = TalhoesGlebas.objects.get_queryset_by_user(self.request_user).order_by('id')
		qs = Adubacao.objects.get_queryset_by_user(self.request_user).order_by('-lote').first()
		
		if qs is None:
			self.fields["lote"].initial = 1
		else:
			lote = qs.lote
			self.fields["lote"].initial = lote + 1

	class Meta:
		model = Adubacao
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'data_adubacao': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'tipo_adubacao': forms.Select(attrs={'class': 'form-control'}),
			'equipamento': forms.Select(attrs={'class': 'form-control'}),
			'talhao_gleba': forms.SelectMultiple(attrs={'class': 'form-control', 'size':'7'}),
			'horas_maquina': forms.NumberInput(attrs={'class': 'form-control'}),
			'insumo': forms.Select(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def clean_lote(self):
		lote = self.cleaned_data['lote']
		if Adubacao.objects.get_queryset_by_user(self.request_user).filter(lote=lote).exists():
			raise ValidationError("Já existe um lote cadastrado com esse valor")
		return lote

	def save(self, commit=True):
		instance = super(AdubacaoForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BaseMdoAdubacaoFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BaseMdoAdubacaoFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BaseMdoAdubacaoFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form

MdoAdubacaoFormSet = inlineformset_factory(
	parent_model = Adubacao,
	model = MdoAdubacao,
	formset = BaseMdoAdubacaoFormSet,
	fk_name='adubacao',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)


class FitossanidadeForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(FitossanidadeForm, self).__init__(*args, **kwargs)

		self.fields["equipamento"].queryset = Equipamento.objects.get_queryset_by_user(self.request_user)
		self.fields["insumo"].queryset = Insumo.objects.get_queryset_by_user(self.request_user).filter(insumo__tipo="2")
		self.fields["talhao_gleba"].queryset = TalhoesGlebas.objects.get_queryset_by_user(self.request_user).order_by('id')
		qs = Fitossanidade.objects.get_queryset_by_user(self.request_user).order_by('-lote').first()
		
		if qs is None:
			self.fields["lote"].initial = 1
		else:
			lote = qs.lote
			self.fields["lote"].initial = lote + 1

	class Meta:
		model = Fitossanidade
		fields = '__all__'
		exclude = ['user','propriedade']
		widgets = {
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'data_fito': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'praga_doenca': forms.TextInput(attrs={'class': 'form-control'}),
			'equipamento': forms.Select(attrs={'class': 'form-control'}),
			'talhao_gleba': forms.SelectMultiple(attrs={'class': 'form-control', 'size':'7'}),
			'horas_maquina': forms.NumberInput(attrs={'class': 'form-control'}),
			'insumo': forms.Select(attrs={'class': 'form-control'}),
			'quantidade': forms.NumberInput(attrs={'class': 'form-control'}),
			'dosagem': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def clean_lote(self):
		lote = self.cleaned_data['lote']
		if Fitossanidade.objects.get_queryset_by_user(self.request_user).filter(lote=lote).exists():
			raise ValidationError("Já existe um lote cadastrado com esse valor")
		return lote

	def save(self, commit=True):
		instance = super(FitossanidadeForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BaseMdoFitossanidadeFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BaseMdoFitossanidadeFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BaseMdoFitossanidadeFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form

MdoFitossanidadeFormSet = inlineformset_factory(
	parent_model = Fitossanidade,
	model = MdoFitossanidade,
	formset = BaseMdoFitossanidadeFormSet,
	fk_name='fitossanidade',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)


class SemeioForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(SemeioForm, self).__init__(*args, **kwargs)

		self.fields["produto"].queryset = Produto.objects.get_queryset_by_user(self.request_user)
		qs = Semeio.objects.get_queryset_by_user(self.request_user).order_by('-lote').first()
		
		if qs is None:
			self.fields["lote"].initial = 1
		else:
			lote = qs.lote
			self.fields["lote"].initial = lote + 1


	class Meta:
		model = Semeio
		fields = '__all__'
		exclude = ['user','propriedade','status_lote']
		widgets = {
			'data_semeio': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'produto': forms.Select(attrs={'class': 'form-control'}),
			'n_bandejas': forms.NumberInput(attrs={'class': 'form-control'}),
			'unidades': forms.NumberInput(attrs={'class': 'form-control', 'onfocus':'soma()'}),
			'viveiro': forms.TextInput(attrs={'class': 'form-control'}),
		}

	def clean_lote(self):
		lote = self.cleaned_data['lote']
		if Semeio.objects.get_queryset_by_user(self.request_user).filter(lote=lote).exists():
			raise ValidationError("Já existe um lote cadastrado com esse valor")
		return lote

	def save(self, commit=True):
		instance = super(SemeioForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade
		instance.status_lote = "1"

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BaseMdoSemeioFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BaseMdoSemeioFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BaseMdoSemeioFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form

MdoSemeioFormSet = inlineformset_factory(
	parent_model = Semeio,
	model = MdoSemeio,
	formset = BaseMdoSemeioFormSet,
	fk_name='semeio',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)


class PlantioForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(PlantioForm, self).__init__(*args, **kwargs)

		self.fields["produto"].queryset = Produto.objects.get_queryset_by_user(self.request_user)
		self.fields["lote_semeio"].queryset = Semeio.objects.get_queryset_by_user(self.request_user).order_by('produto')
		self.fields["talhao_gleba"].queryset = TalhoesGlebas.objects.get_queryset_by_user(self.request_user).order_by('id')
		self.fields["unidades"].required = True
		self.fields["lote"].initial = ''
		
		qs = Plantio.objects.get_queryset_by_user(self.request_user).order_by('-lote').first()
		
		if qs is None:
			self.fields["lote"].initial = 1
		else:
			lote = qs.lote
			self.fields["lote"].initial = lote + 1


	class Meta:
		model = Plantio
		fields = '__all__'
		exclude = ['user','propriedade','status_lote']
		widgets = {
			'data_plantio': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'origem': forms.Select(attrs={'class': 'form-control'}),
			'produto': forms.Select(attrs={'class': 'form-control'}),
			'lote_semeio': forms.Select(attrs={'class': 'form-control'}),
			'talhao_gleba': forms.SelectMultiple(attrs={'class': 'form-control', 'size':'7'}),
			'tipo_plantio': forms.Select(attrs={'class': 'form-control'}),
			'unidades': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def clean_lote(self):
		lote = self.cleaned_data['lote']
		if Plantio.objects.get_queryset_by_user(self.request_user).filter(lote=lote).exists():
			raise ValidationError("Já existe um lote cadastrado com esse valor")
		return lote	

	def save(self, commit=True):
		instance = super(PlantioForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade
		instance.status_lote = "1"

		if instance.produto is None:
			instance.produto = instance.lote_semeio.produto

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BaseMdoPlantioFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BaseMdoPlantioFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BaseMdoPlantioFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form

MdoPlantioFormSet = inlineformset_factory(
	parent_model = Plantio,
	model = MdoPlantio,
	formset = BaseMdoPlantioFormSet,
	fk_name='plantio',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)


class ColheitaForm(forms.ModelForm):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(ColheitaForm, self).__init__(*args, **kwargs)

		self.fields["lote_plantio"].queryset = Plantio.objects.get_queryset_by_user(self.request_user).order_by('produto','-lote','-data_plantio')
		self.fields["lote"].initial = ''

	class Meta:
		model = Colheita
		fields = '__all__'
		exclude = ['user','propriedade','status_lote']
		widgets = {
			'data_colheita': forms.DateInput(attrs={'class': 'date-picker form-control'}),
			'lote': forms.NumberInput(attrs={'class': 'form-control'}),
			'tipo_unidade': forms.Select(attrs={'class': 'form-control'}),
			'lote_plantio': forms.Select(attrs={'class': 'form-control'}),
			'unidades': forms.NumberInput(attrs={'class': 'form-control'}),
		}

	def save(self, commit=True):
		instance = super(ColheitaForm, self).save(commit=False)
		
		instance.user = self.request_user
		instance.data_cadastro = timezone.now()
		instance.propriedade = self.request_user.perfilusuario.propriedade
		instance.status_lote = "1"

		if commit:
			instance.save()
			self.save_m2m()
		return instance


class BaseMdoColheitaFormSet(forms.BaseInlineFormSet):

	def __init__(self, *args, **kwargs):
		self.request_user = kwargs.pop('request_user')
		super(BaseMdoColheitaFormSet, self).__init__(*args, **kwargs)

	def _construct_form(self, *args, **kwargs):

		form = super(BaseMdoColheitaFormSet, self)._construct_form(*args, **kwargs)
		form.fields["nome"].queryset = MaoDeObra.objects.get_queryset_by_user(self.request_user)

		return form

MdoColheitaFormSet = inlineformset_factory(
	parent_model = Colheita,
	model = MdoColheita,
	formset = BaseMdoColheitaFormSet,
	fk_name='colheita',
	fields = '__all__',
	widgets = {
			'nome': forms.Select(attrs={'class': 'form-control'}),
			'horas': forms.NumberInput(attrs={'class': 'form-control'}),
		},
	extra=1)