from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib import admin

from . import views

urlpatterns = [

	url(r'^preparosolo_cadastrar/$', views.preparosolo_cadastrar, name='preparosolo_cadastrar'),
	url(r'^preparosolo_listar/$', views.preparosolo_listar, name='preparosolo_listar'),
	url(r'^preparosolo_editar/(?P<id>[-\w]+)/$', views.preparosolo_editar, name='preparosolo_editar'),
	url(r'^preparosolo_remover/(?P<id>[-\w]+)/$', views.preparosolo_remover, name='preparosolo_remover'),

	url(r'^adubacao_cadastrar/$', views.adubacao_cadastrar, name='adubacao_cadastrar'),
	url(r'^adubacao_listar/$', views.adubacao_listar, name='adubacao_listar'),
	url(r'^adubacao_editar/(?P<id>[-\w]+)/$', views.adubacao_editar, name='adubacao_editar'),
	url(r'^adubacao_remover/(?P<id>[-\w]+)/$', views.adubacao_remover, name='adubacao_remover'),

	url(r'^fitossanidade_cadastrar/$', views.fitossanidade_cadastrar, name='fitossanidade_cadastrar'),
	url(r'^fitossanidade_listar/$', views.fitossanidade_listar, name='fitossanidade_listar'),
	url(r'^fitossanidade_editar/(?P<id>[-\w]+)/$', views.fitossanidade_editar, name='fitossanidade_editar'),
	url(r'^fitossanidade_remover/(?P<id>[-\w]+)/$', views.fitossanidade_remover, name='fitossanidade_remover'),

	url(r'^semeio_cadastrar/$', views.semeio_cadastrar, name='semeio_cadastrar'),
	url(r'^semeio_listar/$', views.semeio_listar, name='semeio_listar'),
	url(r'^semeio_remover/(?P<id>[-\w]+)/$', views.semeio_remover, name='semeio_remover'),
	url(r'^semeio_editar/(?P<id>[-\w]+)/$', views.semeio_editar, name='semeio_editar'),
	url(r'^semeio_fechar_abrir/(?P<id>[-\w]+)/$',  views.semeio_fechar_abrir, name='semeio_fechar_abrir'),

	url(r'^ajax_celulas/(?P<id_produto>[-\w]+)/$', views.ajax_celulas, name='ajax_celulas'),

	url(r'^plantio_cadastrar/$', views.plantio_cadastrar, name='plantio_cadastrar'),
	url(r'^plantio_listar/$', views.plantio_listar, name='plantio_listar'),
	url(r'^plantio_relatorio/$', views.plantio_relatorio, name='plantio_relatorio'),
	url(r'^plantio_remover/(?P<id>[-\w]+)/$', views.plantio_remover, name='plantio_remover'),
	url(r'^plantio_editar/(?P<id>[-\w]+)/$', views.plantio_editar, name='plantio_editar'),
	url(r'^plantio_fechar_abrir/(?P<id>[-\w]+)/$',  views.plantio_fechar_abrir, name='plantio_fechar_abrir'),

	url(r'^ajax_produto/(?P<id>[-\w]+)/$', views.ajax_produto, name='ajax_produto'),
	# url(r'^ajax_origem/(?P<origem>[-\w]+)/$', views.ajax_origem, name='ajax_origem'),
	# url(r'^ajax_lote_prop/(?P<id>[-\w]+)/$', views.ajax_lote_prop, name='ajax_lote_prop'),

	url(r'^colheita_cadastrar/$', views.colheita_cadastrar, name='colheita_cadastrar'),
	url(r'^colheita_listar/$', views.colheita_listar, name='colheita_listar'),
	url(r'^colheita_relatorio/$', views.colheita_relatorio, name='colheita_relatorio'),
	url(r'^colheita_remover/(?P<id>[-\w]+)/$', views.colheita_remover, name='colheita_remover'),
	url(r'^colheita_editar/(?P<id>[-\w]+)/$', views.colheita_editar, name='colheita_editar'),
	url(r'^colheita_fechar_abrir/(?P<id>[-\w]+)/$',  views.colheita_fechar_abrir, name='colheita_fechar_abrir'),

	url(r'^ajax_lote_prop_colheita/(?P<id>[-\w]+)/$', views.ajax_lote_prop_colheita, name='ajax_lote_prop_colheita'),

	url(r'^talhaogleba_detalhe/(?P<talhao>[\d]+)/$', views.talhaogleba_detalhe, name='talhaogleba_detalhe'),

]
