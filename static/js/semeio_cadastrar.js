
$(document).ready(function() {

    $("#id_produto").change(function() {
        var id_produto = $("select#id_produto option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/ciclo/ajax_celulas/" + id_produto + "/",
            success: function(data) {
                $("#celulas_bandeja").val(data);
            }
        });

    });

});
