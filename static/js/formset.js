var initFormSets = function(options){

    // ----------------------------------------------- FormSet Initial -------------------------------------------------------//
    options.initialForms = {};
    options.totalForms = {};

    for (var index in options.prefixes){

        var formset_class = options.prefixes[index];
        // options.initialForms[formset_class] = parseInt($('#id_'+formset_class+'_set-TOTAL_FORMS').val());
        options.initialForms[formset_class] = parseInt($('#id_'+formset_class+'-TOTAL_FORMS').val());
        options.totalForms[formset_class] = options.initialForms[formset_class];

        for (var field = 0; field < options.totalForms[formset_class]; field++){
           $('#id_'+ formset_class + '_set-' + field + '-DELETE, label[for="id_' + formset_class + '_set-' + field + '-DELETE"]').hide();
        }
    };

    // ----------------------------------------------- FormSet Functions -----------------------------------------------//
    $('.addForm').on('click', function(){
        var formset = $(this).data('formset'); 
        addForm(formset);
    });

    function addForm(form_class){ 
        if (options.totalForms[form_class] < options.initialForms[form_class]){
            var fields = $('.' + form_class + '_row .wrapper').filter(':hidden').first();
            $('[id*="DELETE"]', fields).prop('checked', false);
            fields.show();
        } else {
            var fields = $('.'+form_class+'_row .wrapper').first().clone();
            updateFieldsAttrs(fields, form_class);
            cleanFields(fields);
            $('.'+form_class+'_row').append(fields);         
        }

        options.totalForms[form_class] = options.totalForms[form_class] + 1;
        if (options.totalForms[form_class] > 0){
            $('.removeForm[data-formset=' + form_class + ']').show()
        }
    };

    function cleanFields(fields){
        $('select, input[type="url"], input[type="text"]', fields).val('');
    }

    function updateFieldsAttrs(fields, form_class){
        // $('[name^="' + form_class + '_set-"], [for^="id_' + form_class + '_set-"] ', fields).each(function(){
        $('[name^="' + form_class + '"], [for^="id_' + form_class + '"] ', fields).each(function(){
            if ($(this).attr('for') !== undefined){
                $(this).attr('for', updateAttrs($(this), 'for', form_class));
            } else{
                $(this).attr('name', updateAttrs($(this), 'name', form_class));
                $(this).attr('id', updateAttrs($(this), 'id', form_class));
            }
        });
    };

    function updateAttrs(field, attr, form_class){
        var field_attr = $(field).attr(attr).split('-');
        field_attr[1] = options.totalForms[form_class];
        return field_attr.join('-');
    };

    $('.removeForm').on('click', function(){
        var formset = $(this).data('formset'); 
        removeForm(formset);
    });

    function removeForm(form_class){
        fields = $('.' + form_class + '_row .wrapper').not(':hidden').last();
        if (options.totalForms[form_class] <= options.initialForms[form_class]){
            $('[id*="DELETE"]', fields).prop('checked', true);
            fields.hide();
        } else{fields.remove();}

        options.totalForms[form_class] = options.totalForms[form_class] - 1;

        if (options.totalForms[form_class] < 1){
            $('.removeForm[data-formset=' + form_class + ']').hide();
        };
     };

    function validateEmptyFields(){

        for (var index in options.prefixes) {
            for (field = 0; field < options.totalForms[options.prefixes[index]]; field++){
                if ($('#id_'+options.prefixes[index]+'_set-'+field+'-plataforma') != undefined){
                    if ($('#id_'+options.prefixes[index]+'_set-'+field+'-plataforma').val() =='' && $('#id_'+options.prefixes[index]+'_set-'+field+'-link').val() == ''){
                        $('#id_'+options.prefixes[index]+'_set-'+field+'-DELETE').prop('checked', true);
                    }
                }else{
                    if ($('#id_'+options.prefixes[index]+'_set-'+field+'-nome').val() =='' && $('#id_'+options.prefixes[index]+'_set-'+field+'-cargo').val() == ''){
                        $('#id_'+options.prefixes[index]+'_set-'+field+'-DELETE').prop('checked', true);
                    }
                }
            }
        }
    };

    // ----------------------------------------------- Form Submition -----------------------------------------------//
    $('form').submit(function(event){
        validateEmptyFields();
        for (var index in options.prefixes){
            if (options.totalForms[options.prefixes[index]] < options.initialForms[options.prefixes[index]]){
                options.totalForms[options.prefixes[index]] = options.initialForms[options.prefixes[index]];
            }
            $('#id_'+options.prefixes[index]+'_set-TOTAL_FORMS').val(options.totalForms[options.prefixes[index]]);
        }
    });

    function validateEmptyFields(){

        for (var index in options.prefixes) {
            for (field = 0; field < options.totalForms[options.prefixes[index]]; field++){
                if ($('#id_'+options.prefixes[index]+field+'-plataforma') != undefined){
                    if ($('#id_'+options.prefixes[index]+field+'-plataforma').val() =='' && $('#id_'+options.prefixes[index]+field+'-link').val() == ''){
                        $('#id_'+options.prefixes[index]+field+'-DELETE').prop('checked', true);
                    }
                }else{
                    if ($('#id_'+options.prefixes[index]+field+'-nome').val() =='' && $('#id_'+options.prefixes[index]+field+'-cargo').val() == ''){
                        $('#id_'+options.prefixes[index]+field+'-DELETE').prop('checked', true);
                    }
                }
            }
        }
    };

        // ----------------------------------------------- Form Submition -----------------------------------------------//
    $('form').submit(function(event){
        validateEmptyFields();
        for (var index in options.prefixes){
            if (options.totalForms[options.prefixes[index]] < options.initialForms[options.prefixes[index]]){
                options.totalForms[options.prefixes[index]] = options.initialForms[options.prefixes[index]];
            }
            $('#id_'+options.prefixes[index]+'-TOTAL_FORMS').val(options.totalForms[options.prefixes[index]]);
        }
    });
    
}