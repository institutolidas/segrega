
$(document).ready(function() {

    $("#id_lote_plantio").change(function() {
        var lote = $("select#id_lote_plantio option:selected").attr("value");

        $.ajax({
            type: "GET",
            url: "/ciclo/ajax_lote_prop_colheita/" + lote + "/",
            success: function(data) {
                $("#id_lote").val(data);
            }
        });

    });

});
